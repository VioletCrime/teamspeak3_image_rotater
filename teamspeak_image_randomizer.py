#!/usr/bin/python

import commands
import datetime
import os
import random
import sys
import telnetlib
import timeout_decorator

IMAGE_LIST_FILE="/home/teamspeak/.ts_image_rotator/image_list.txt"
#IMAGE_LIST_MASTER_FILE=""
IMAGE_LIST_LOG_DIR="/home/teamspeak/.ts_image_rotator/logs/"
TELNET_HOST="localhost"
TELNET_PORT=10011
TELNET_LOGIN="serveradmin"
TELNET_PASS="lolwat"
TS_SID=1

class Image(object) :
    def __init__(self, raw_entry) :
        self.raw_entry = raw_entry
        self.sections = self.raw_entry.split(' : ')
        self.URL = self.sections[0]
        self.datestamp = None
        if len(self.sections) > 1 :
            self.datestamp = self.sections[1]

    def __repr__(self) :
        result = self.URL
        if self.datestamp is not None :
            result += " : " + self.datestamp
        return result

    def update_time(self, date) :
        self.datestamp = repr(date.year) + "/" + repr(date.month) + "/" + repr(date.day)

class ImageList(object) :
    def __init__(self, input_file = IMAGE_LIST_FILE) :
        self.valid = True

        now = datetime.datetime.now()
        self.today = datetime.date(now.year, now.month, now.day)
        if os.path.isfile(input_file) :
            with open(input_file) as phyle :
                raw_list = phyle.readlines()
            self.image_list = []
            for entry in raw_list :
                self.image_list.append(Image(entry.replace('\n', '')))
        else :
            self.valid = False
            return

    def update_image_list(self, output_file = IMAGE_LIST_FILE) :
        with open(output_file, 'w') as phyle :
            for entry in self.image_list :
                if entry.datestamp is not None :
                    phyle.write(entry.URL + " : " + entry.datestamp + "\n")
                else :
                    phyle.write(entry.URL + "\n")

    def select_random_image(self) :
        index = random.randint(0, len(self.image_list))
        self.image_list[index].update_time(self.today)
        return self.image_list[index]

    def verify_date_entries(self) :
        for image in self.image_list :
            if image.datestamp is None :
                try_viewing_image(image)

    def refresh_nearly_expired_images(self) :
        for image in self.image_list :
            date_vals = image.datestamp.split('/')
            last_viewed = datetime.date(int(date_vals[0]), int(date_vals[1]), int(date_vals[2]))
            if self.today >= last_viewed + datetime.timedelta(days=150) :
                self.try_viewing_image(image)

    def update_all_date_entries(self) :
        for image in self.image_list :
            self.try_viewing_image(image)

    def try_viewing_image(self, image) :
        try :
            self.view_image(image)
            image.update_time(self.today)
        except timeout_decorator.TimeoutError :
            print "View timed out! Will need to update later!"

    @timeout_decorator.timeout(5)
    def view_image(self, image) :
        print "Viewing URL: %s" % (image.URL)
        commands.getoutput("curl -o /dev/null %s" % (image.URL))

class Manager(object) :
    def __init__(self) :
        self.image_list = ImageList()

    def run(self) :
        if not self.image_list.valid :
            return 1
        if len(sys.argv) > 1 :
            if sys.argv[1] == "--2501" :
                self.image_list.update_all_date_entries()
                self.image_list.update_image_list()
                return 0
        new_image = self.image_list.select_random_image()
        self.modify_ts_image(new_image.URL)
        self.image_list.try_viewing_image(new_image)
        self.run_maintenance()
        return 0

    def modify_ts_image(self, new_URL) :
        print new_URL
        tn = telnetlib.Telnet(TELNET_HOST, TELNET_PORT)
        tn.write("login %s %s\n" % (TELNET_LOGIN, TELNET_PASS))
        tn.write("use sid=%s\n" % (TS_SID))
        tn.write("serveredit virtualserver_hostbanner_gfx_url=%s\n" % (new_URL))
        telnet_output = tn.read_very_eager()
        print telnet_output
        tn.close()

    def run_maintenance(self) :
        self.image_list.verify_date_entries()
        self.image_list.refresh_nearly_expired_images()
        # TODO Detect deleted images
        # TODO? Detect new images from master list?
        self.image_list.update_image_list()

if __name__ == "__main__" :
    manager = Manager()
    sys.exit(manager.run())
